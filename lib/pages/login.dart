import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn _googleSignIn = new GoogleSignIn();
FirebaseUser user;

class LoginPage extends StatefulWidget {
  final String title;

  const LoginPage({Key key, this.title}) : super(key: key);
  @override
  _MyLoginPageState createState() => new _MyLoginPageState();
}

class _MyLoginPageState extends State<LoginPage> {
  Future<String> _avatar = new Future<String>.value('');

  Future<String> _testSignInAnonymously() async {
    user = await _auth.signInAnonymously();
    assert(user != null);
    assert(user.isAnonymous);
    assert(!user.isEmailVerified);
    assert(await user.getIdToken() != null);
    if (Platform.isIOS) {
      // Anonymous auth doesn't show up as a provider on iOS
      assert(user.providerData.isEmpty);
    } else if (Platform.isAndroid) {
      // Anonymous auth does show up as a provider on Android
      assert(user.providerData.length == 1);
      assert(user.providerData[0].providerId == 'firebase');
      assert(user.providerData[0].uid != null);
      assert(user.providerData[0].displayName == null);
      assert(user.providerData[0].photoUrl == null);
      assert(user.providerData[0].email == null);
    }

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);

    return 'signInAnonymously succeeded: $user';
  }

  Future<String> _testSignInWithGoogle() async {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;
    user = await _auth.signInWithGoogle(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    assert(user.email != null);
    assert(user.displayName != null);
    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);

    return user.photoUrl;
  }

  String _testSignOutWithGoogle() {
    _googleSignIn.signOut();
    return ("Sign Out");
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('App Name'),
      ),
      body: new Center(
        child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Expanded(
                flex: 1,
                child: new Container(
                  decoration:
                      BoxDecoration(shape: BoxShape.circle, color: Colors.red),
                  padding: const EdgeInsets.all(2.0),
                  // child: _avatar != null ? Image.network(_avatar) : null,
                ),
              ),
              new Expanded(
                flex: 2,
                child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                        padding: const EdgeInsets.all(2.0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            new MaterialButton(
                                child: const Text('Test signInAnonymously'),
                                onPressed: () {
                                  setState(() {
                                    // _message = _testSignInAnonymously();
                                  });
                                }),
                          ],
                        ),
                      ),
                      Container(
                        child: new MaterialButton(
                            child: const Text('Test signInWithGoogle'),
                            onPressed: () {
                              setState(() {
                                _avatar = _testSignInWithGoogle();
                              });
                            }),
                      ),
                      Container(
                        child: new MaterialButton(
                            child: const Text('Test signOutWithGoogle'),
                            onPressed: () {
                              print(_testSignOutWithGoogle());
                            }),
                      ),
                      Container(
                        child: new FutureBuilder<String>(
                            future: _avatar,
                            builder: (_, AsyncSnapshot<String> snapshot) {
                              return new Text(snapshot.data ?? '',
                                  style: const TextStyle(
                                      color: const Color.fromARGB(
                                          255, 0, 155, 0)));
                            }),
                      )
                    ]),
              )
            ]),
      ),
    );
  }
}
